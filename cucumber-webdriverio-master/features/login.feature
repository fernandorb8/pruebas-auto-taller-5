#Complete siguiendo las instrucciones del taller.
# Feature: Login into losestudiantes
#    As an user I want to authenticate myself within losestudiantes website in order to rate teachers
#
# Scenario: Login failed
#    Given I go to losestudiantes home screen
#    When I open the login screen
#    And I fill a wrong email and password
#    And I try to login
#    Then I expect to not be able to login

Feature: Login into losestudiantes
    As an user I want to authenticate myself within losestudiantes website in order to rate teachers

Scenario Outline: Login failed with wrong inputs

  Given I go to losestudiantes home screen
  When I open the login screen
  And I fill with <email> and <password>
  And I try to login
  Then I expect to see <error>

  Examples:
    | email            | password | error                    |
    |                  |          | Ingresa una contraseña   |
    | miso@gmail.com   |    1234  | Upss! El correo y        |

Scenario: Login successful

  Given I go to losestudiantes home screen
  When I open the login screen
  And I fill with fernandorb_8@hotmail.com and 123456789abcd
  And I try to login
  Then I expect to have the login icon
