Feature: Defining a trip.

	Scenario: As a user I want to define a Transmi trip the first time I open the App.
		#Remove splash screen.
		Given I press "Stations" 
		Given I press "Travel in Transmi, SITP or Taxi"
		#Remove splash screen.
		Given I press "Transmilenio"
		#Remove splash screen.
		Given I press "Transmilenio"
		Given I press "Your location"
		Then I enter "Aguas" into input field number 1
		Given I press "Las Aguas"
		Given I press "End"
		Then I enter "Portal" into input field number 1
		Given I press "Portal Norte"
		#Falis depending on assigned button index. 
		Then I press image button number 2
		Then I see the text "Recommendation 1"
		
	Scenario: As a user I want to define a SITP trip the second time I open the App.
		Given I press "Travel in Transmi, SITP or Taxi"
		Given I press "SITP"
		Given I press "Your location"
		Then I enter "Estación Universidades" into input field number 1
		Given I press "Estación Universidades"
		Given I press "End"
		Then I enter "AK 15 - AC 116" into input field number 1
		Given I press "Avenida Calle 116"
		#Falis depending on assigned button index. 
		Then I press image button number 2
		Then I see the text "Recommendation 1"
		
	Scenario: As a user I want to define a SITP trip the third time I open the App. Having registered my taxi account.
		Given I press "Travel in Transmi, SITP or Taxi"
		Given I press "Taxi"
		Given I press "Log in with email"
		Then I enter "fernandorb_8@hotmail.com" into input field number 1
		#Calabash-android no detecta el texto cuando se usa la vista de todo en mayúscula.
		Given I press "Next"
		Then I enter "123456789a" into input field number 2
		Given I press "Sign In"
		Given I press "Taxi"				
		Then I see the text "Propina"
		